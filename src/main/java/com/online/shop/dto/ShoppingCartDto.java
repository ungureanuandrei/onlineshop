package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class ShoppingCartDto {
    private List<ShoppingCartItemDto> items;
    private String total;
    private String subTotal;
    private String totalNumberOfProducts;
    public ShoppingCartDto(){
        items = new ArrayList<>(); //declaram un constructor unde initializam variabila items
    }
    public void add(ShoppingCartItemDto shoppingCartItemDto){
        items.add(shoppingCartItemDto);
    }
}
