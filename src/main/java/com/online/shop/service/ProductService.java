package com.online.shop.service;

import com.online.shop.dto.ChosenProductDto;
import com.online.shop.dto.ProductDto;
import com.online.shop.entities.Product;
import com.online.shop.mapper.ProductMapper;
import com.online.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;
    public void addProduct(ProductDto productDto, MultipartFile multipartFile) {
        Product product = productMapper.map(productDto, multipartFile);
        productRepository.save(product);
    }
    public List<ProductDto> getAllProductDtos() {
        List<Product> productList = productRepository.findAll();
        List<ProductDto> productDtoList = new ArrayList<>();
        for (Product product : productList) {//in loc de foreach se poate folosi stream sau iterator
            ProductDto productDto = productMapper.map(product);
            productDtoList.add(productDto);
        }
        return productDtoList;
    }
    public Optional<ProductDto> getOptionalProductDtoByID(String productId) {
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if(optionalProduct.isEmpty()){
            return Optional.empty();
        }
        Product product = optionalProduct.get();
        ProductDto productDto = productMapper.map(product);
        return Optional.of(productDto);
    }
    public void adjustStock(ChosenProductDto chosenProductDto, String productId){
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        Product product = optionalProduct.get();
        Integer adjustedQuantity = Integer.valueOf(chosenProductDto.getQuantity());
        Integer adjustedStock = product.getQuantity() + adjustedQuantity;
        if(adjustedStock >=0){
            product.setQuantity(adjustedStock);
            productRepository.save(product);
        }
    }
}
