package com.online.shop.validator;

import com.online.shop.dto.ChosenProductDto;
import com.online.shop.entities.Product;
import com.online.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;

@Service
public class ChosenProductValidator {
    @Autowired
    private ProductRepository productRepository;
    public void validate(ChosenProductDto chosenProductDto, String productId, BindingResult bindingResult){
        try{
            Integer quantity = Integer.valueOf(chosenProductDto.getQuantity());
            Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
            if(quantity <=0){
                FieldError fieldError =new FieldError("chosenProductDto","quantity",
                        "Product quantity must be positive!");
                bindingResult.addError(fieldError);
            } else if(quantity > optionalProduct.get().getQuantity()){
                FieldError fieldError = new FieldError("chosenProductDto","quantity",
                        "Requested quantity is not in stock!");
                bindingResult.addError(fieldError);
            }
        } catch(NumberFormatException exception){
            FieldError fieldError = new FieldError("chosenProductDto","quantity",
                    "Product quantity is not a number!");
            bindingResult.addError(fieldError);
        }
    }
    public void validateAdjustment(ChosenProductDto chosenProductDto, String productId,
                                   BindingResult bindingResult){
        try{
            Integer quantity = Integer.valueOf(chosenProductDto.getQuantity());
            Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
            if(quantity<0 && (0-quantity) > optionalProduct.get().getQuantity()){
                FieldError fieldError = new FieldError("chosenProductDto", "quantity",
                        "Can not reduce the stock with more than it is available!");
                bindingResult.addError(fieldError);
            }
        }catch(NumberFormatException exception){
            FieldError fieldError = new FieldError("chosenProductDto","quantity",
                    "Product quantity is not a number!");
            bindingResult.addError(fieldError);
        }
    }
}
